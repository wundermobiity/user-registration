<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentInformationTable extends Migration
{
    public function up(): void
    {
        Schema::create('payment_information', function(Blueprint $table): void {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('account_owner', 100);
            $table->string('iban', 39);
            $table->text('payment_data_id');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('payment_information');
    }
}
