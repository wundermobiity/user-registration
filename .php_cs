<?php

use PhpCsFixer\Config;
use PhpCsFixer\Finder;

/*
 * Define folders to fix
 */
$finder = Finder::create()
    ->in([
        __DIR__ . '/app',
        __DIR__ . '/config',
        __DIR__ . '/tests',
        __DIR__ . '/database',
        __DIR__ . '/routes',
        __DIR__ . '/resources/lang',
        __DIR__ . '/resources/views/',
    ])
    ->filter(function (SplFileInfo $fileInfo) {
        $exclude = [
//            __DIR__ .'/app/Http/api.php' => true,
//            __DIR__ .'/app/Http/routes.php' => true,
//            __DIR__ .'/app/Http/breadcrumbs.php' => true,
        ];

        if (isset($exclude[$fileInfo->getPathname()])) {
            return false;
        }

        return true;
    });

/*
 * Do the magic
 */
return Config::create()
    ->setUsingCache(false)
    // This is only so that CircleCI can cache this file
    ->setCacheFile(__DIR__ . '/vendor/.php_cs.cache')
    ->setRiskyAllowed(true)
    ->setRules([
//        '@Symfony' => true,
//        '@Symfony:risky' => true,


//        '@PSR2'                               => true,
//        'method_argument_space'               => [
//            'ensure_fully_multiline' => false
//        ],
        'phpdoc_add_missing_param_annotation' => ['only_untyped' => false],
        'phpdoc_order'                        => true,
        'no_short_echo_tag'                   => true,
//        'semicolon_after_instruction'         => true,
        'align_multiline_comment'             => true,
        'ordered_imports'                     => true,
        'strict_param'                        => true,
        'declare_strict_types'                => true,
        'array_syntax'                        => ['syntax' => 'short'],
        'linebreak_after_opening_tag'         => true,

        'strict_param' => true,
        'declare_strict_types' => true,
        'mb_str_functions' => true,
        'heredoc_to_nowdoc' => true,
        'linebreak_after_opening_tag' => true,
        'native_function_invocation' => true,
        'no_multiline_whitespace_before_semicolons' => true,
        'no_unreachable_default_argument_value' => true,
        'no_useless_else' => true,
        'no_useless_return' => true,
        'phpdoc_add_missing_param_annotation' => [
            'only_untyped' => true
        ],
        'phpdoc_order' => true,
        'random_api_migration' => true,
        'simplified_null_return' => true,
        'ternary_to_null_coalescing' => true,
        'void_return' => true,
        'ordered_imports' => true,
        // Changed
        'no_whitespace_in_blank_line' => true,
        'class_attributes_separation' => [
            'elements' => ['method', 'property']
        ],
        'blank_line_before_statement' => [
            'statements' => ['return', 'throw', 'try', 'declare']
        ],
        'array_syntax' => ['syntax' => 'short'],
        'concat_space' => [
            'spacing' => 'one'
        ],
        'phpdoc_types_order' => [
            'null_adjustment' => 'always_last'
        ],
        'visibility_required' => [
            'elements' => ['property', 'method', 'const']
        ],
        'function_declaration' => [
            'closure_function_spacing' => 'none'
        ],
        // Excluded
        'trailing_comma_in_multiline_array' => false,
        'blank_line_after_opening_tag' => false,
        'phpdoc_align' => false

    ])
    ->setFinder($finder);
