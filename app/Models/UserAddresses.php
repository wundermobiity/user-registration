<?php
declare(strict_types=1);


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

final class UserAddresses extends Model
{
    protected $fillable = [
        'user_id',
        'address',
        'house_number',
        'zip_code',
        'city',
    ];
}
