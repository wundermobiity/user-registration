<?php
declare(strict_types=1);


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

final class PaymentInformation extends Model
{
    protected $fillable = ['user_id', 'iban', 'payment_data_id', 'account_owner'];
}
