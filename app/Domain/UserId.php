<?php
declare(strict_types=1);


namespace App\Domain;


use App\Identities\IntegerIdentity;

final class UserId extends IntegerIdentity
{

}
