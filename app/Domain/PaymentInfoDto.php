<?php
declare(strict_types=1);


namespace App\Domain;


use App\Identities\StringIdentity;

final class PaymentInfoDto
{
    private StringIdentity $ownerName;

    private StringIdentity $iban;

    private UserId $customerId;

    public function __construct(StringIdentity $ownerName, StringIdentity $iban, UserId $customerId)
    {
        $this->ownerName = $ownerName;
        $this->iban = $iban;
        $this->customerId = $customerId;
    }

    public function ownerName(): StringIdentity
    {
        return $this->ownerName;
    }

    public function iban(): StringIdentity
    {
        return $this->iban;
    }

    public function customerId(): UserId
    {
        return $this->customerId;
    }
}
