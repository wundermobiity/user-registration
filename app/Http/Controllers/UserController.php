<?php
declare(strict_types=1);


namespace App\Http\Controllers;


final class UserController
{
    public function logout() {
        auth()->logout();

        return redirect('/');
    }
}
