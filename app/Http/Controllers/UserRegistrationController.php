<?php
declare(strict_types=1);


namespace App\Http\Controllers;


use App\Domain\PaymentInfoDto;
use App\Domain\UserId;
use App\Http\Requests\GetPaymentDataInfoRequest;
use App\Identities\StringIdentity;
use App\Models\PaymentInformation;
use App\Models\User;
use App\Models\UserAddresses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

final class UserRegistrationController extends Controller
{
    private GetPaymentDataInfoRequest $paymentDataInfoRequest;

    public function __construct(GetPaymentDataInfoRequest $paymentDataInfoRequest)
    {
        $this->paymentDataInfoRequest = $paymentDataInfoRequest;
    }

    public function userRegistration()
    {
        return view('user-registration.registration-form');
    }

    public function storeStepOne(Request $request)
    {
        $userModel = User::create($request->all());
        Auth::login($userModel);

        return redirect()->route('user-registration.step-two')->with('success', ['User data saved successfully', 'success']);
    }

    public function userRegistrationStepTwo()
    {
        return view('user-registration.address');
    }

    public function storeStepTwo(Request $request)
    {
        $address = $request->all();
        $address['user_id'] = Auth::user()->id;
        UserAddresses::create($address);

        return redirect()->route('user-registration.step-three')->with('success', ['Address information saved successfully', 'success']);
    }

    public function userRegistrationStepThree()
    {
        return view('user-registration.payment-information');
    }

    public function storeStepThree(Request $request)
    {
        $paymentInfo = $request->all();

        $paymentDataId = $this->paymentDataInfoRequest->getDataId(
          new PaymentInfoDto(
              new StringIdentity($paymentInfo['account_owner']),
              new StringIdentity($paymentInfo['iban']),
              new UserId(Auth::user()->id)
          )
        );
        $paymentInfo['user_id'] = Auth::user()->id;
        $paymentInfo['payment_data_id'] = $paymentDataId->paymentDataId()->toString();

        PaymentInformation::create($paymentInfo);

        return redirect()->route('user-registration.success')->with('success', ['Payment information saved successfully', 'success']);
    }

    public function userRegistrationSuccess(Request $request)
    {
        $paymentInfo = Auth::user()->paymentInfo->first();

        return view('user-registration.success', \compact('paymentInfo'));
    }
}
