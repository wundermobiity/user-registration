<?php
declare(strict_types=1);


namespace App\Http\Controllers\Api;


use App\Models\User;
use Fig\Http\Message\StatusCodeInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

final class PhoneCheckController
{
    private User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function exists(Request $request): JsonResponse
    {
        $existingPhone = User::Where('phone', $request->input('phone'))->first();

        $exists = false;
        if($existingPhone instanceof User) {
            $exists = true;
        }

        return new JsonResponse(['exists' => $exists], StatusCodeInterface::STATUS_OK);
    }
}
