<?php
declare(strict_types=1);


namespace App\Http\Response;


use App\Identities\StringIdentity;

final class PaymentInfoResponse
{
    private StringIdentity $paymentDataId;

    public function __construct(StringIdentity $paymentDataId)
    {
        $this->paymentDataId = $paymentDataId;
    }

    public function paymentDataId(): StringIdentity
    {
        return $this->paymentDataId;
    }
}
