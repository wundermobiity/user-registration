<?php
declare(strict_types=1);


namespace App\Http\Middleware;


use App\Models\PaymentInformation;
use App\Models\User;
use App\Models\UserAddresses;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserIsAuthorized
{
    public function handle(Request $request, Closure $next, ...$guards)
    {

        if(!Auth::check()){
            return redirect(route('user-registration'));
        }

        return $next($request);
    }
}
