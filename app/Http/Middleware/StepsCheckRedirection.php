<?php
declare(strict_types=1);


namespace App\Http\Middleware;


use App\Models\PaymentInformation;
use App\Models\User;
use App\Models\UserAddresses;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StepsCheckRedirection
{
    public function handle(Request $request, Closure $next, ...$guards)
    {

        if(Auth::check()){

            /** @var User $user */
            $user = Auth::user();
            /** @var UserAddresses $userAddress */
            $userAddress = Auth::user()->addresses;
            if(!empty($userAddress)){
                $userAddress = $userAddress->first();
            }
            /** @var PaymentInformation $paymentInfo */
            $paymentInfo = Auth::user()->paymentInfo;
            if(!empty($paymentInfo)){
                $paymentInfo = $paymentInfo->first();
            }

            if(!empty($user->first_name) && !empty($user->last_name) && !empty($user->phone) && !request()->has('r')) {
                return redirect(route('user-registration.step-two', ['r' => 1]));
            } else if (!empty($userAddress->address) && !empty($userAddress->house_number) && !empty($userAddress->zip_code) && !empty($userAddress->city) && request()->input('r') < 2){
                return redirect(route('user-registration.step-three', ['r' => 3]));
            } else if (!empty($paymentInfo->account_owner) && !empty($paymentInfo->iban) && request()->input('r') < 4){
                return redirect(route('user-registration.success', ['r' => 4]));
            }
        }

        return $next($request);
    }
}
