<?php
declare(strict_types=1);


namespace App\Http\Requests;

use App\Domain\PaymentInfoDto;
use App\Http\Response\PaymentInfoResponse;
use App\Identities\StringIdentity;
use Fig\Http\Message\StatusCodeInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;

final class GetPaymentDataInfoRequest
{
    private Client $client;

    private const SERVICE_ENDPOINT = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';

    /**
     * @throws GuzzleException
     */
    public function getDataId(PaymentInfoDto $paymentInfoDto): PaymentInfoResponse
    {
        $client = new Client(['headers' => ['Accept' => 'application/json']]);

        try {
            $response = $client->post(self::SERVICE_ENDPOINT, [
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
                'body' => \json_encode([
                    'customerId' => $paymentInfoDto->customerId()->id(),
                    'iban' => $paymentInfoDto->iban()->toString(),
                    'owner' => $paymentInfoDto->ownerName()->toString()
                ])
            ]);
            $statusCode = $response->getStatusCode();

            if ($statusCode !== StatusCodeInterface::STATUS_OK) {
                throw new \Exception('Error occurred while fetching paymentDataId ');
            }
            $responseData = \json_decode($response->getBody()->getContents(), false);

            return new PaymentInfoResponse(
                new StringIdentity($responseData->paymentDataId)
            );

        } catch (GuzzleException | \Exception $e) {
            Log::error($e->getMessage());

            throw new $e;
        }
    }
}
