<?php
declare(strict_types=1);

namespace App\Identities;

/**
 * Interface Identity
 * @package TC\Persistence\Identity
 */
interface Identity
{
    /**
     * @param Identity $identity
     * @return bool
     */
    public function equals(Identity $identity) : bool;

    /**
     * @return string
     */
    public function toString() : string;
}
