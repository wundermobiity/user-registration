<?php
declare(strict_types=1);

namespace App\Identities;

class IntegerIdentity implements Identity
{
    /** @var int  */
    private $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function equals(Identity $identity) : bool
    {
        if (false === $identity instanceof static) {
            return false;
        }

        return $this->id === $identity->id();
    }

    public function id() : int
    {
        return $this->id;
    }

    public function toString(): string
    {
        return (string) $this->id;
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
