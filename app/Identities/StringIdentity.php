<?php
declare(strict_types=1);

namespace App\Identities;

class StringIdentity implements Identity
{
    /** @var string  */
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function equals(Identity $identity) : bool
    {
        if (false === $identity instanceof static) {
            return false;
        }

        return $this->id === $identity->id();
    }

    public function id() : string
    {
        return $this->id;
    }

    public function toString(): string
    {
        return $this->id;
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
