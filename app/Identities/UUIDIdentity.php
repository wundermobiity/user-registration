<?php
declare(strict_types=1);

namespace App\Identities;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class UUIDIdentity implements Identity
{
    /** @var UuidInterface */
    private $uuid;

    public function __construct(UuidInterface $uuid = null)
    {
        $this->uuid = $uuid ?? Uuid::uuid4();
    }

    public function equals(Identity $identity): bool
    {
        if (false === $identity instanceof static) {
            return false;
        }

        return $this->uuid->toString() === $identity->toString();
    }

    public function id(): UuidInterface
    {
        return $this->uuid;
    }

    public function toString(): string
    {
        return $this->uuid->toString();
    }

    public function __toString()
    {
        return $this->toString();
    }
}
