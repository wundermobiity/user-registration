@extends('layout.app')

@section('content')
    {{ Form::open(['route' => ['user-registration.store-step-two'], 'id' => 'user-info-form', 'method' => 'post', 'autocomplete' => 'off']) }}
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {{ Form::label('address', 'Address') }}
                {{ Form::text('address', old('address'), ['required' => 'required', 'class' => 'form-control'.$errors->first('address', ' is-invalid'), 'autofocus']) }}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {{ Form::label('house_number', 'House Number') }}
                {{ Form::text('house_number', old('house_number'), ['maxlength' => 5, 'required' => 'required', 'class' => 'form-control'.$errors->first('house_number', ' is-invalid'), 'autofocus']) }}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {{ Form::label('zip_code', 'Zip Code') }}
                {{ Form::text('zip_code', old('zip_code'), ['required' => 'required', 'class' => 'form-control'.$errors->first('zip_code', ' is-invalid'), 'autofocus']) }}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {{ Form::label('city', 'City') }}
                {{ Form::text('city', old('city'), ['required' => 'required', 'class' => 'form-control'.$errors->first('city', ' is-invalid'), 'autofocus']) }}
            </div>
        </div>
    </div>
    <span class="btn-group float-right"><button id="save-button" type="submit" class="btn btn-primary">Save</button></span>
    {{ Form::close() }}
@endsection

@section('js')
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="{{asset('js/form-validation.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#save-button').click(function (e) {

                e.preventDefault();
                if (validateInput()) {
                    alert('All fields are required!');
                    return;
                }
                $('#user-info-form').submit();
            });
        });
    </script>
@endsection
