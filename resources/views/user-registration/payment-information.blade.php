@extends('layout.app')

@section('content')
    {{ Form::open(['route' => ['user-registration.store-step-three'], 'id'=> 'user-info-form', 'method' => 'post', 'autocomplete' => 'off']) }}
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {{ Form::label('account_owner', 'Account Owner') }}
                {{ Form::text('account_owner', old('account_owner'), ['required' => 'required', 'class' => 'form-control'.$errors->first('account_owner', ' is-invalid'), 'autofocus']) }}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {{ Form::label('iban', 'IBAN') }}
                {{ Form::text('iban', old('iban'), ['required' => 'required', 'class' => 'form-control'.$errors->first('iban', ' is-invalid'), 'autofocus']) }}
            </div>
        </div>
    </div>
    <span class="btn-group float-right"><button id="save-button" type="submit"
                                                class="btn btn-primary">Save</button></span>
    {{ Form::close() }}
@endsection
@section('js')
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="{{asset('js/form-validation.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#save-button').click(function (e) {

                e.preventDefault();
                if (validateInput()) {
                    alert('All fields are required!');
                    return;
                }
                $('#user-info-form').submit();
            });
        });

    </script>
@endsection
