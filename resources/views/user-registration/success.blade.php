@extends('layout.app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            You have successfully saved data !!!
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            Your payment Id: {{ \Illuminate\Support\Facades\Auth::user()->paymentInfo->first()->payment_data_id }}
        </div>
    </div>
@stop
