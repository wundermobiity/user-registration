@extends('layout.app')

@section('content')
    {{ Form::open(['route' => ['user-registration.store-step-one'], 'id' => 'user-registration-form', 'method' => 'post', 'autocomplete' => 'off']) }}
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {{ Form::label('first_name', 'First name') }}
                {{ Form::text('first_name', old('first_name'), ['required' => 'required', 'class' => 'form-control'.$errors->first('first_name', ' is-invalid'), 'autofocus']) }}
                {!! $errors->first('first_name','<div class="error-bubble"><div>:message</div></div>') !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {{ Form::label('last_name', 'Last name') }}
                {{ Form::text('last_name', old('last_name'), ['required' => 'required', 'class' => 'form-control'.$errors->first('last_name', ' is-invalid'), 'autofocus']) }}
                {!! $errors->first('last_name','<div class="error-bubble"><div>:message</div></div>') !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {{ Form::label('phone', 'Phone') }}
                {{ Form::text('phone', old('phone'), ['id' => 'phone', 'required' => 'required', 'class' => 'form-control'.$errors->first('phone', ' is-invalid'), 'autofocus']) }}
                {!! $errors->first('phone','<div class="error-bubble"><div>:message</div></div>') !!}
            </div>
        </div>
    </div>
    <span class="btn-group float-right"><button type="submit" id="save-button"
                                                class="btn btn-primary">Save</button></span>
    {{ Form::close() }}
@endsection

@section('js')
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="{{asset('js/form-validation.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#save-button').click(function (e) {

                e.preventDefault();
                if (validateInput()) {
                    alert('All fields are required!');
                    return;
                }
                $.ajax({
                    method: "GET",
                    url: "{{ route('check-phone') }}",
                    data: {phone: $('#phone').val()}
                }).done(function (msg) {
                    if (msg.exists === true) {
                        alert('Phone number already exists in databasse!');
                        $('#phone').focus();
                        return;
                    }
                    $('#user-registration-form').submit();
                });
            });
        });
    </script>
@endsection
