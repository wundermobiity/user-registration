<!doctype html>
<html>
<head>
    @include('layout.partial.head')
</head>
<body>
<div class="container">
    <header class="row">
        @include('layout.partial.header')
    </header>
    <div id="main" class="row">
        <section class="content">
            @yield('content')
        </section>
    </div>
    <footer class="row">
    </footer>
</div>
</body>
@yield('js')
</html>
