<div class="navbar">
    <div class="navbar-inner">
        <ul class="nav">
            @if(\Illuminate\Support\Facades\Auth::check())
            <li><a href="{{ route('logout') }}">Logout</a></li>
            @endif
        </ul>
    </div>
</div>
