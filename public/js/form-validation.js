
function validateInput() {
    let isSuccess = false;
    $('input[type=text]').each(function () {
        if ($(this).val() === '') {
            isSuccess = true;
        }
    });
    return isSuccess;
}
