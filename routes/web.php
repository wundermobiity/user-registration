<?php
declare(strict_types=1);

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return redirect('user-registration');
});

Route::group(['middleware' => ['steps-checking']], function(): void {
    Route::get('/user-registration', ['as' => 'user-registration', 'uses' => 'UserRegistrationController@userRegistration']);
    Route::post('/user-registration/step-one', ['as' => 'user-registration.store-step-one', 'uses' => 'UserRegistrationController@storeStepOne']);
    Route::get('/user-registration/step-two', ['as' => 'user-registration.step-two', 'uses' => 'UserRegistrationController@userRegistrationStepTwo']);
    Route::get('/user-registration/step-three', ['as' => 'user-registration.step-three', 'uses' => 'UserRegistrationController@userRegistrationStepThree']);
    Route::get('/user-registration/success', ['as' => 'user-registration.success', 'uses' => 'UserRegistrationController@userRegistrationSuccess']);
});

Route::get('/logout', ['as' => 'logout', 'uses' => 'UserController@logout']);

Route::post('/user-registration/step-one', ['as' => 'user-registration.store-step-one', 'uses' => 'UserRegistrationController@storeStepOne']);
Route::post('/user-registration/step-two', ['as' => 'user-registration.store-step-two', 'uses' => 'UserRegistrationController@storeStepTwo']);
Route::post('/user-registration/step-three', ['as' => 'user-registration.store-step-three', 'uses' => 'UserRegistrationController@storeStepThree']);
