
# User registration app

application was developed using PHP7.4

##instalation:

to fetch all packages that are required for application to run, run in application root

    composer install

in .env file change database connection settings (line 10-15)

database dump file with db structure is in root of project and it is named 

    user_registration-2021_02_10_21_51_41.sql

Javascript is used on first step as phone unique value in db validation


nginx site example:

    server {
        listen 80;
        listen 443 ssl http2;
        server_name myapp.local;
        root "/provide/path/to/project/public/folder";
    
        index index.html index.htm index.php;
    
        charset utf-8;
    
        location / {
            try_files $uri $uri/ /index.php?$query_string;
            
        }
    
        location = /favicon.ico { access_log off; log_not_found off; }
        location = /robots.txt  { access_log off; log_not_found off; }
    
        access_log off;
    
        sendfile off;
    
        location ~ \.php$ {
            fastcgi_split_path_info ^(.+\.php)(/.+)$;
            fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
            fastcgi_index index.php;
            include fastcgi_params;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            
    
            fastcgi_intercept_errors off;
            fastcgi_buffer_size 16k;
            fastcgi_buffers 4 16k;
            fastcgi_connect_timeout 300;
            fastcgi_send_timeout 300;
            fastcgi_read_timeout 300;
        }
    
        location ~ /\.ht {
            deny all;
        }
    }
